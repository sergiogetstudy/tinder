package com.tinder.service;

import com.tinder.dao.credentials.ICredentialDAO;
import com.tinder.dao.credentials.CredentialsSqlDAO;
import com.tinder.domain.UserPass;

import java.util.Optional;

public class CredentialsService {
    ICredentialDAO dao;

    public CredentialsService(CredentialsSqlDAO dao) {
        this.dao = dao;
    }

    public Optional<UserPass> readByLoginPass(String login, String password) {
        try {
            return this.dao.readByLoginPass(login, password);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
