package com.tinder.service;

import com.tinder.dao.message.MessageSqlDAO;
import com.tinder.domain.Message;

import java.util.List;

public class MessagesService {
    private final MessageSqlDAO dao;

    public MessagesService(MessageSqlDAO dao) {
        this.dao = dao;
    }

    public boolean create(Message m) {
        return dao.create(m);
    }

    public boolean update(Message m) {
        return dao.updateContent(m);
    }

    public boolean delete(int id) {
        return dao.delete(id);
    }

    public List<Message> readSome(int sender, int receiver, int offset, int limit) {
        return dao.readSomeOf(sender, receiver, offset, limit);
    }
}
