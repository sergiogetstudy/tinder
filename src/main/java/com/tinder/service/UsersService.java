package com.tinder.service;

import com.tinder.dao.user.UserSqlDAO;
import com.tinder.domain.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UsersService {
    UserSqlDAO dao;

    public UsersService(UserSqlDAO dao) {
        this.dao = dao;
    }

    public List<User> getAllUsers() throws SQLException {
        return dao.readAll();
    }

    public Optional<User> getUserToLike(User currentUser) throws SQLException {
        return dao.readUserToLike(currentUser);
    }

    public List<User> getLikedUsers(User currentUser) throws SQLException {
        return dao.readLikedUsers(currentUser);
    }

    public Optional<User> getUserById(int id) {
        return dao.read(id);
    }

    public boolean updateLastActivityById(int id) {
        return dao.updateLastActivity(id);
    }
}
