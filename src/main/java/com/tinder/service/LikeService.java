package com.tinder.service;

import com.tinder.dao.like.LikeSqlDAO;
import com.tinder.domain.Like;

public class LikeService {
    private LikeSqlDAO dao;

    public LikeService(LikeSqlDAO dao) {
        this.dao = dao;
    }

    public void saveNewLike(Like like) throws RuntimeException {
        if (!this.dao.create(like)) throw new RuntimeException("Something went wrong! Data wasn`t saved");
    }

    public void createNewLike(int likedBy_userid, int liked_userid, boolean is_liked) throws RuntimeException {
        saveNewLike(new Like(likedBy_userid, liked_userid, is_liked));
    }
}
