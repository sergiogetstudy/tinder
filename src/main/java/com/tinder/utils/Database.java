package com.tinder.utils;

import org.postgresql.ds.PGPoolingDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class Database {
    private static final String serverName = "ec2-34-251-233-253.eu-west-1.compute.amazonaws.com";
    private static final String databaseName = "d7a9nvrkic4pa2";
    private static final String user = "uldhsftxzdkmer";
    private static final String password = "de447fd4a71d557df96e29d02a794d528710d1800d2867768882f0f3c7ced26c";
    private static final int maxConnections = 20;


    public static Connection conn() throws SQLException {
        PGPoolingDataSource source = new PGPoolingDataSource();
        source.setServerName(serverName);
        source.setDatabaseName(databaseName);
        source.setUser(user);
        source.setPassword(password);
        source.setMaxConnections(maxConnections);

        return source.getConnection();
    }

}
