package com.tinder.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class DbUtils {

    public static <A> List<A> convertToList(ResultSet rs, FunctionEX<ResultSet, A> f) throws SQLException {
        LinkedList<A> list = new LinkedList<>();
        while (rs.next()) {
            A item = f.apply(rs);
            list.add(item);
        }
        return list;
    }

    public static <A> Optional<A> convertToOptional(ResultSet rs, FunctionEX<ResultSet, A> f) throws SQLException {
        if (rs.next()) {
            return Optional.of(f.apply(rs));
        }
        return Optional.empty();
    }
}
