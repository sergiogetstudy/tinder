package com.tinder;

import com.tinder.controller.*;
import com.tinder.dao.credentials.CredentialsSqlDAO;
import com.tinder.dao.like.LikeSqlDAO;
import com.tinder.dao.user.UserSqlDAO;
import com.tinder.dao.message.MessageSqlDAO;
import com.tinder.service.CredentialsService;
import com.tinder.service.LikeService;
import com.tinder.service.MessagesService;
import com.tinder.service.UsersService;
import com.tinder.utils.Database;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import jakarta.servlet.DispatcherType;


import java.sql.Connection;
import java.util.EnumSet;

public class Launcher {
    public static void main(String[] args) throws Exception {

        //get port
        String portStr = System.getenv("PORT");
        portStr = portStr == null ? "8080" : portStr;
        Integer port = Integer.parseInt(portStr);
        System.out.println("PORT: " + port);

        //start server
        Server server = new Server(port);

        //create connection object
        Connection connection = Database.conn();

        //create template engine object
        TemplateEngine te = new TemplateEngine();

        //create services
        CredentialsService cs = new CredentialsService(new CredentialsSqlDAO(connection));
        UsersService us = new UsersService(new UserSqlDAO(connection));
        LikeService ls = new LikeService(new LikeSqlDAO(connection));
        MessagesService ms = new MessagesService(new MessageSqlDAO(connection));

        SessionHandler sessionHandler = new SessionHandler();

        ServletContextHandler handler = new ServletContextHandler();

        handler.setSessionHandler(sessionHandler);
        handler.addServlet(new ServletHolder(new LoginServlet(te)), "/login");
        handler.addFilter(new FilterHolder(new LoginFilter(te, cs, us)), "/*", EnumSet.of(DispatcherType.REQUEST));
        handler.addServlet(new ServletHolder(new UsersServlet(te, ls, us)), "/users");
        handler.addServlet(new ServletHolder(new LikedServlet(te, us)), "/liked");
        handler.addServlet(new ServletHolder(new FileServlet()), "/assets/*");
        handler.addServlet(new ServletHolder(new MessagesServlet(te, ms, us)), "/messages/*");

        server.setHandler(handler);
        server.join();
        server.start();
    }
}
