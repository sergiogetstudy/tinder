package com.tinder.dao.credentials;

import com.tinder.domain.UserPass;

import java.util.List;
import java.util.Optional;

public interface ICredentialDAO {
    public Optional<UserPass> readByLoginPass(String login, String password) throws Exception;

    boolean create(UserPass a);

    Optional<UserPass> read(int id) throws Exception;

    List<UserPass> readAll() throws Exception;
}
