CREATE TABLE credentials(
                            id int GENERATED ALWAYS AS IDENTITY NOT NULL CONSTRAINT PK_userpass PRIMARY KEY,
                            user_id int NOT NULL,
                            login varchar(25) NOT NULL,
                            password varchar(25) NOT NULL,

                            CONSTRAINT FK_userpass_user FOREIGN KEY(user_id) REFERENCES users(id)
);

INSERT INTO credentials(user_id, login, password) VALUES (3, 'tanya@gmail.com', 'tanya1234');

SELECT * FROM credentials