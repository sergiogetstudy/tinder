package com.tinder.dao.credentials;

import com.tinder.domain.UserPass;
import com.tinder.utils.DbUtils;

import java.sql.*;
import java.util.List;
import java.util.Optional;

public class CredentialsSqlDAO implements ICredentialDAO {
    Connection connection;

    public CredentialsSqlDAO(Connection connection) {
        this.connection = connection;


    }

    public boolean create(UserPass userPass) {
        try {
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO credentials(id, user_id, login, password) VALUES (?, ?, ?, ?)");
            preparedStatement.setInt(1, userPass.id());
            preparedStatement.setInt(2, userPass.getUserId());
            preparedStatement.setString(3, userPass.getLogin());
            preparedStatement.setString(4, userPass.getPassword());

            int executionResult = preparedStatement.executeUpdate();
            connection.commit();

            return executionResult > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }

    public Optional<UserPass> read(int id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM credentials WHERE id = ?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        return DbUtils.convertToOptional(resultSet, UserPass.createFromDB());
    }

    public List<UserPass> readAll() throws Exception {
        PreparedStatement ps = connection.prepareStatement("""
                 SELECT id, user_id, login, password
                 FROM credentials
                 )
                """);
        ResultSet rs = ps.executeQuery();
        return DbUtils.convertToList(rs, UserPass.createFromDB());
    }

    public Optional<UserPass> readByLoginPass(String login, String password) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM credentials " +
                "WHERE login=? AND password=?");
        ps.setString(1, login);
        ps.setString(2, password);
        ResultSet resultSet = ps.executeQuery();

        return DbUtils.convertToOptional(resultSet, UserPass.createFromDB());
    }
}
