package com.tinder.dao.message;

import com.tinder.dao.message.IMessageDAO;
import com.tinder.domain.Message;
import com.tinder.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class MessageSqlDAO implements IMessageDAO {

    private final Connection conn;

    public MessageSqlDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public boolean create(Message m) {
        try {
            PreparedStatement ps = conn.prepareStatement("""
                INSERT INTO messages (
                    sender_id, receiver_id, content, time
                )
                VALUES (
                    ?, ?, ?, ?
                )
                """);
            ps.setInt(1, m.sId());
            ps.setInt(2, m.rId());
            ps.setString(3, m.content());
            ps.setTimestamp(4, m.time());
            return ps.execute();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<Message> readSomeOf(int sId, int rId, int offset, int limit) {
        try {
            PreparedStatement ps = conn.prepareStatement("""
                    SELECT
                        m.id,
                        m.sender_id,
                        s.name AS sender_name,
                        s.image AS sender_img,
                        m.receiver_id,
                        r.name AS receiver_name,
                        r.image AS receiver_img,
                        m.content,
                        m.time
                    FROM messages m
                    JOIN users s ON m.sender_id = s.id
                    JOIN users r ON m.receiver_id = r.id
                    WHERE (m.sender_id = ? AND m.receiver_id = ?)
                       OR (m.sender_id = ? AND m.receiver_id = ?)
                    ORDER BY m.time
                    OFFSET ?
                    LIMIT ?
                    """);
            ps.setInt(1, sId);
            ps.setInt(2, rId);
            ps.setInt(3, rId);
            ps.setInt(4, sId);
            ps.setInt(5, offset);
            ps.setInt(6, limit);
            ResultSet rs = ps.executeQuery();
            return DbUtils.convertToList(rs, Message.createFromDB());
        } catch (Exception e) {
            return List.of();
        }
    }

    @Override
    public boolean updateContent(Message m) {
        try {
            PreparedStatement ps = conn.prepareStatement("""
                    UPDATE messages
                    SET
                        content = ?
                    WHERE id = ?
                    """);
            ps.setString(1, m.content());
            ps.setInt(2, m.id());
            return ps.execute();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean delete(int id) {
        try {
            PreparedStatement ps = conn.prepareStatement("""
                DELETE FROM messages WHERE id = ?
                """);
            ps.setInt(1, id);
            return ps.execute();
        } catch (Exception e) {
            return false;
        }
    }
}
