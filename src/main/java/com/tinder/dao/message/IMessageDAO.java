package com.tinder.dao.message;

import com.tinder.domain.Message;

import java.util.List;

public interface IMessageDAO {
    boolean create(Message m);
    List<Message> readSomeOf(int sId, int rId, int offset, int limit);
    boolean updateContent(Message m);
    boolean delete(int id);
}
