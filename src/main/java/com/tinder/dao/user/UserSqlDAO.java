package com.tinder.dao.user;

import com.tinder.dao.user.IUserDAO;
import com.tinder.domain.SEX;
import com.tinder.domain.User;
import com.tinder.utils.DbUtils;

import java.sql.*;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

public class UserSqlDAO implements IUserDAO {
    Connection connection;

    public UserSqlDAO(Connection connection) {
        this.connection = connection;
    }

    public Optional<User> read(int id) {
        try {
            PreparedStatement ps = connection.prepareStatement("""
                    SELECT 
                    id, name, profession, image, last_activity, sex 
                    FROM users
                    WHERE id = ?
                    """);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return DbUtils.convertToOptional(rs, User.createFromDB());
        } catch (Exception ex) {
            return Optional.empty();
        }
    }

    public List<User> readAll() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("""
                        SELECT 
                        id, name, profession, image, last_activity, sex 
                        FROM users
                        """);
        ResultSet rs = ps.executeQuery();
        return DbUtils.convertToList(rs, User.createFromDB());
    }

    public Optional<User> readUserToLike(User currentUser) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("""
                SELECT id, name, profession, image, last_activity, sex
                FROM users
                WHERE id != ? AND sex::text = ?::text
                  AND id NOT IN (
                    SELECT liked_userid
                    FROM likes
                    WHERE likedby_userid = ?
                  )
                  LIMIT 1
                """);
        ps.setInt(1, currentUser.id());
        ps.setString(2, currentUser.getSex() == SEX.female ? "male" : "female");
        ps.setInt(3, currentUser.id());
        ResultSet rs = ps.executeQuery();
        return DbUtils.convertToOptional(rs, User.createFromDB());
    }

    public List<User> readLikedUsers(User currentUser) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("""
                 SELECT id, name, profession, image, last_activity, sex
                 FROM users
                 WHERE id IN (
                     SELECT liked_userid
                     FROM likes
                     WHERE likedby_userid = ? 
                     AND is_liked = true
                 )
                """);
        ps.setInt(1, currentUser.id());
        ResultSet rs = ps.executeQuery();
        return DbUtils.convertToList(rs, User.createFromDB());
    }

    public boolean updateLastActivity(int id) {
        try {
            PreparedStatement ps = connection.prepareStatement("""
                    UPDATE users
                    SET last_activity = ?
                    WHERE id = ?
                    """);

            Timestamp ts = Timestamp.from(Instant.now());
            ps.setTimestamp(1, ts);
            ps.setInt(2, id);
            ps.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
