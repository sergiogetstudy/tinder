package com.tinder.dao.user;

import com.tinder.domain.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IUserDAO {
    Optional<User> read(int id);

    List<User> readAll() throws SQLException;

    Optional<User> readUserToLike(User currentUser) throws SQLException;

    List<User> readLikedUsers(User currentUser) throws SQLException;

    boolean updateLastActivity(int id);
}
