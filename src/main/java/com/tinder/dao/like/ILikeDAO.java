package com.tinder.dao.like;

import com.tinder.domain.Like;

public interface ILikeDAO {
    boolean create(Like a);
}
