package com.tinder.dao.like;

import com.tinder.dao.like.ILikeDAO;
import com.tinder.domain.Like;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class LikeSqlDAO implements ILikeDAO {

    private Connection connection;

    public LikeSqlDAO(Connection connection) {
        this.connection = connection;
    }

    public boolean create(Like a) {
        try {
            PreparedStatement ps = connection.prepareStatement("""
                    INSERT INTO likes (likedBy_userID, liked_userID, is_liked)
                    VALUES (?, ?, ?)""");
            ps.setInt(1, a.getLikedBy_userId());
            ps.setInt(2, a.getLiked_userId());
            ps.setBoolean(3, a.isLiked());
            ps.executeUpdate();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
