package com.tinder.controller;

import com.tinder.domain.Message;
import com.tinder.domain.User;
import com.tinder.service.MessagesService;
import com.tinder.service.UsersService;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

public class MessagesServlet extends HttpServlet {
    private final TemplateEngine te;
    private final MessagesService ms;
    private final UsersService us;

    public MessagesServlet(TemplateEngine te, MessagesService ms, UsersService us) {
        this.te = te;
        this.ms = ms;
        this.us = us;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int sender = ((User) req.getSession().getAttribute("user")).id();
        int receiver = Integer.parseInt(req.getParameter("id"));

        Map<String, Object> data = new HashMap<>();
        try {
            Optional<User> ou = us.getUserById(receiver);
            if (ou.isPresent()) {
                List<Message> ml = ms.readSome(sender, receiver, 0, 20);
                data.put("receiver", ou.get());
                data.put("messages", ml);
                te.render("chat.ftl", data, resp);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int sId = ((User) req.getSession().getAttribute("user")).id();
            int rId = Integer.parseInt(req.getParameter("id"));
            String content = req.getParameter("message");
            Timestamp t = new Timestamp(new Date().getTime());
            ms.create(Message.toStore(sId, rId, content, t));
            resp.sendRedirect(String.format("/messages?id=%s", rId));
        } catch (Exception e) {
            resp.sendError(500, e.getMessage());
        }
    }
}
