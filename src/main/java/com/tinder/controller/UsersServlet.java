package com.tinder.controller;

import com.tinder.domain.User;
import com.tinder.service.LikeService;
import com.tinder.service.UsersService;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class UsersServlet extends HttpServlet {
    TemplateEngine templateEngine;
    UsersService user_service;
    LikeService like_service;
    public UsersServlet(TemplateEngine templateEngine, LikeService like_service, UsersService usersService) {
        this.templateEngine = templateEngine;
        this.user_service = usersService;
        this.like_service = like_service;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, Object> data = new HashMap<>();
        try {
            User currentUser = (User) req.getSession().getAttribute("user");
            User user = user_service.getUserToLike(currentUser).orElseThrow(
                    () -> new SQLException("WE don`t have any users to like")
            );
            data.put("user", user);
            templateEngine.render("like-page.ftl", data, resp);
        } catch (SQLException ex) {
            resp.sendRedirect("/liked");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            User currentUser = (User) req.getSession().getAttribute("user");
            int likedUserID = Integer.parseInt(req.getParameter("userId"));
            boolean isLiked = Boolean.parseBoolean(req.getParameter("isLiked"));
            this.like_service.createNewLike(currentUser.id(), likedUserID, isLiked);
            resp.sendRedirect("/users");
        } catch (Exception ex) {
            resp.sendError(500, ex.getMessage());
        }
    }
}
