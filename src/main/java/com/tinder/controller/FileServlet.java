package com.tinder.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (ServletOutputStream os = resp.getOutputStream()) {
            String rqName = req.getRequestURI();
            ClassLoader classLoader = this.getClass().getClassLoader();

            byte[] bytes = classLoader.getResourceAsStream(rqName.substring(1)).readAllBytes();
            os.write(bytes);

        } catch (NullPointerException ex) {
            resp.setStatus(404);
        }
    }
}
