package com.tinder.controller;

import com.tinder.domain.User;
import com.tinder.domain.UserPass;
import com.tinder.service.CredentialsService;
import com.tinder.service.UsersService;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

public class LoginFilter implements Filter {
    private TemplateEngine templateEngine;
    private CredentialsService credentialsService;
    private UsersService usersService;

    public LoginFilter(TemplateEngine te, CredentialsService cs, UsersService us) {
        this.templateEngine = te;
        this.credentialsService = cs;
        this.usersService = us;
    }

    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        Set<String> allowedUrls = Set.of("/assets", "/assets/style.css", "/assets/bootstrap.min.css");

        if (allowedUrls.contains(request.getServletPath()) ) {
            chain.doFilter(request, response);
        }


        if (request.getSession(false) == null) {
            String login = request.getParameter("email");
            String password = request.getParameter("password");

            Optional<UserPass> userPass = credentialsService.readByLoginPass(login, password);
            if (userPass.isPresent()) {
                HttpSession session = request.getSession();
                UserPass up = userPass.get();
                User u = usersService.getUserById(up.getUserId()).get();
                usersService.updateLastActivityById(u.id());
                session.setAttribute("userPass", up);
                session.setAttribute("user", u);

                chain.doFilter(request, response);
            }
            if (((HttpServletRequest) req).getRequestURI().equals("/login")) {
                templateEngine.render("login.ftl", response);
                return;
            }
            response.sendRedirect("/login");
        }
        chain.doFilter(request, response);
    }
}
