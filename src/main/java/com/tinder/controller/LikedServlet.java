package com.tinder.controller;

import com.tinder.domain.User;
import com.tinder.service.UsersService;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LikedServlet extends HttpServlet {
    TemplateEngine templateEngine;
    UsersService user_service;

    public LikedServlet(TemplateEngine templateEngine, UsersService user_service) {
        this.templateEngine = templateEngine;
        this.user_service = user_service;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, Object> data = new HashMap<>();
        try {
            User currentUser = (User) req.getSession().getAttribute("user");
            List<User> likedUsers = user_service.getLikedUsers(currentUser);
            if (likedUsers.size() == 0) resp.sendRedirect("/users");
            else {
                data.put("users", likedUsers);
                templateEngine.render("people-list.ftl", data, resp);
            }
        } catch (SQLException ex) {
            resp.sendError(500, ex.getMessage());
        }
    }
}
