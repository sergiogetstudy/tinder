package com.tinder.domain;

import com.tinder.utils.FunctionEX;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Message implements Identifiable {

    private final Integer id;
    private final int sId;
    private final String sName;
    private final String sImg;
    private final int rId;
    private final String rName;
    private final String rImg;
    private final Timestamp time;
    private final String content;

    private Message(Integer id, int sId, String sName, String sImg, int rId, String rName, String rImg, String content, Timestamp time) {
        this.id = id;
        this.sId = sId;
        this.sName = sName;
        this.sImg = sImg;
        this.rId = rId;
        this.rName = rName;
        this.rImg = rImg;
        this.time = time;
        this.content = content;
    }

    public static Message toStore(int sId, int rId, String content, Timestamp time) {
        return new Message(null, sId, null, null, rId, null, null, content, time);
    }
    public static Message toDisplay(int id, int sId, String sName, String sImg, int rId, String rName, String rImg, String content, Timestamp time) {
        return new Message(id, sId, sName, sImg, rId, rName, rImg, content, time);
    }

    @Override
    public int id() {
        return id;
    }

    public int sId() {
        return sId;
    }

    public String sName() {
        return sName;
    }

    public String sImg() {
        return sImg;
    }

    public int rId() {
        return rId;
    }

    public String rName() {
        return rName;
    }

    public String rImg() {
        return rImg;
    }

    public Timestamp time() {
        return time;
    }

    public String content() {
        return content;
    }

    public String prettyTime() {
        return new SimpleDateFormat("MMM dd, h:mm a").format(time);
    }

    public static FunctionEX<ResultSet, Message> createFromDB() {
        return (ResultSet rs) -> Message.toDisplay(
                rs.getInt("id"),
                rs.getInt("sender_id"),
                rs.getString("sender_name"),
                rs.getString("sender_img"),
                rs.getInt("receiver_id"),
                rs.getString("receiver_name"),
                rs.getString("receiver_img"),
                rs.getString("content"),
                rs.getTimestamp("time")
        );
    }
}
