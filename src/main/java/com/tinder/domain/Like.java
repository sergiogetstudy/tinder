package com.tinder.domain;

public class Like implements Identifiable {
    private Integer id;
    private int likedBy_userId;
    private int liked_userId;

    public int getLikedBy_userId() {
        return likedBy_userId;
    }

    public int getLiked_userId() {
        return liked_userId;
    }

    public boolean isLiked() {
        return isLiked;
    }

    private boolean isLiked;

    public Like(Integer id, int likedBy_userId, int liked_userId, boolean isLiked) {
        this.id = id;
        this.likedBy_userId = likedBy_userId;
        this.liked_userId = liked_userId;
        this.isLiked = isLiked;
    }

    public Like(int likedBy_userId, int liked_userId, boolean isLiked) {
        this(null, likedBy_userId, liked_userId, isLiked);
    }

    @Override
    public int id() {
        return id;
    }
}
