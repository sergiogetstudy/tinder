package com.tinder.domain;

import com.tinder.utils.FunctionEX;

import java.sql.ResultSet;

public class UserPass implements Identifiable {
    private int id;
    private int userId;
    private String login;
    private String password;

    public UserPass(int id, int userId, String login, String password) {
        this.id = id;
        this.userId = userId;
        this.login = login;
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int id() {
        return this.id;
    }

    public static FunctionEX<ResultSet, UserPass> createFromDB() {
        return (ResultSet rs) -> new UserPass(
                rs.getInt("id"),
                rs.getInt("user_id"),
                rs.getString("login"),
                rs.getString("password")
        );
    }
}
