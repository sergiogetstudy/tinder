package com.tinder.domain;

public interface Identifiable {
    int id();
}
