package com.tinder.domain;

import com.tinder.utils.FunctionEX;

import java.sql.ResultSet;
import java.time.*;
import java.time.chrono.ChronoPeriod;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class User  implements Identifiable{
    private Integer id;
    private String name;
    private String profession;
    private SEX sex;
    private String image;
    private Instant last_activity;

    public User(int id, String name, String profession, String sex, String image, Instant last_activity) {
        this.id = id;
        this.name = name;
        this.profession = profession;
        this.sex = sex.equals(SEX.male.name()) ? SEX.male : SEX.female;
        this.image = image;
        this.last_activity = last_activity;
    }

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getName() {
        return name;
    }

    public String getProfession() {
        return profession;
    }

    public SEX getSex() {
        return sex;
    }

    public String getImage() {
        return image;
    }

    public Instant getLast_activity() {
        return last_activity;
    }

    @Override
    public int id() {
        return id;
    }

    public String getLastActivityDateFormated() {
        LocalDateTime dateTime = LocalDateTime.ofInstant(last_activity, ZoneId.systemDefault());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return dateTime.format(formatter);
    }

    public String getLastActivityPeriodFormatted() {
        LocalDateTime startDateTime = LocalDateTime.ofInstant(last_activity, ZoneOffset.UTC);
        LocalDateTime endDateTime = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);

        LocalDate startDate = startDateTime.toLocalDate();
        LocalDate endDate = endDateTime.toLocalDate();

        long days = ChronoUnit.DAYS.between(startDate, endDate);
        return days > 0 ? String.format("%d days ago", days) : "today";
    }

    public static FunctionEX<ResultSet, User> createFromDB() {
        return (ResultSet rs) -> new User(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getString("profession"),
                rs.getString("sex"),
                rs.getString("image"),
                rs.getTimestamp("last_activity").toInstant()
        );
    }
}
