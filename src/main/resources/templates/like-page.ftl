<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../assets/favicon.png">

    <title>Like page</title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/assets/style.css">
</head>
<body style="background-color: #f5f5f5;">

<div class="col-lg-6 offset-lg-3 col-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 text-center">
                    <div class="avatar-img-l"><img src="${user.image}" alt="" class="img-circle-l"></div>
                    <h3 class="mb-0 text-truncated">${user.name}</h3>
                    <br>
                </div>
                <div class="col-6">
                    <form action="/users" method="POST">
                        <input type="hidden" name="userId" value="${user.id()}">
                        <input type="hidden" name="isLiked" value="false">
                        <button type="submit" class="btn btn-outline-danger btn-block">
                            <span class="fa fa-times"></span> Dislike
                        </button>
                    </form>
                </div>
                <div class="col-6">
                    <form action="/users" method="POST">
                        <input type="hidden" name="userId" value="${user.id()}">
                        <input type="hidden" name="isLiked" value="true">
                        <button type="submit" class="btn btn-outline-success btn-block">
                            <span class="fa fa-heart"></span> Like
                        </button>
                    </form>
                </div>
                <!--/col-->
            </div>
            <!--/row-->
        </div>
        <!--/card-block-->
    </div>
</div>

</body>
</html>
