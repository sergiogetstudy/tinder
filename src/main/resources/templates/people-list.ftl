<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../assets/favicon.png">

    <title>People list</title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/assets/style.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-8 offset-lg-2 col-12 ">
            <div class="panel panel-default user_panel">
                <div class="panel-heading">
                    <h3 class="panel-title">User List</h3>
                </div>
                <div class="panel-body">
                    <div class="table-container">
                        <table class="table-users table" border="0">
                            <tbody>
                            <#list users as user>
                                <tr onclick="redirectToUserPage('${user.id()}')">
                                    <td width="10">
                                        <div class="avatar-img-s">
                                            <img class="img-circle-s" src="${user.image}"/>
                                        </div>

                                    </td>
                                    <td class="align-middle">
                                        ${user.name}
                                    </td>
                                    <td class="align-middle">
                                        ${user.profession}
                                    </td>
                                    <td class="align-middle">
                                        Last Login: ${user.getLastActivityDateFormated()}<br>
                                        <small class="text-muted">${user.getLastActivityPeriodFormatted()}</small>
                                    </td>
                                </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function redirectToUserPage(userId) {
        window.location.href = "/messages?id=" + userId;
    }
</script>

</body>
</html>
